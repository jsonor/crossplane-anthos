# crossplane-anthos



## Provisionning du 1er *cluster* `Anthos` depuis `CrossPlane`

### Démarrage de `Minikube`
:warning: `CrossPlane` est assez gourmand en ressources. À moins de 4 Go de RAM, `Minikube` s'avère instable.

```bash
minikube start
kubectl get nodes
kubectl get namespaces
```

### Cluster GKE
```bash
gcloud container clusters get-credentials crossplane-bootstrap --location europe-west1
```

### Poser des variables d'environnement
```bash
gcloud config set core/project lgu-demos
export GCP_ZONE=europe-west1-b
export GCP_REGION=europe-west1
export GITLAB_USERNAME=laurentgrangeau
export GCP_PROJECT_ID=`gcloud config get-value core/project`
export GCP_PROJECT_NUMBER=`gcloud projects describe ${GCP_PROJECT_ID} --format json | jq --raw-output '.projectNumber'`
```

### Création d'un service account `GCP`
On créé un *service account*, qui sera utilisé par les outils d'*Infra-as-Code*.
```bash
GCP_SERVICE_ACCOUNT_NAME=crossplane-anthos-sa
GCP_SERVICE_ACCOUNT="${GCP_SERVICE_ACCOUNT_NAME}@${GCP_PROJECT_ID}.iam.gserviceaccount.com"
gcloud iam service-accounts create ${GCP_SERVICE_ACCOUNT_NAME} --project ${GCP_PROJECT_ID}
```

On donne les droits nécessaires au *service account*.
```bash
# role for GCP bucket creation
gcloud projects add-iam-policy-binding --role="roles/storage.admin" ${GCP_PROJECT_ID} --member "serviceAccount:${GCP_SERVICE_ACCOUNT}"

# role for GKE cluster creation
gcloud projects add-iam-policy-binding --role="roles/container.clusterAdmin" ${GCP_PROJECT_ID} --member "serviceAccount:${GCP_SERVICE_ACCOUNT}"
gcloud projects add-iam-policy-binding --role="roles/iam.serviceAccountUser" ${GCP_PROJECT_ID} --member "serviceAccount:${GCP_SERVICE_ACCOUNT}"
gcloud projects add-iam-policy-binding --role="roles/compute.instanceAdmin.v1" ${GCP_PROJECT_ID} --member "serviceAccount:${GCP_SERVICE_ACCOUNT}"

# role for GKE cluster registration into an Anthos fleet
gcloud projects add-iam-policy-binding --role="roles/gkehub.admin" ${GCP_PROJECT_ID} --member "serviceAccount:${GCP_SERVICE_ACCOUNT}"

# role for Cloud SQL creation
gcloud projects add-iam-policy-binding --role="roles/cloudsql.admin" ${GCP_PROJECT_ID} --member "serviceAccount:${GCP_SERVICE_ACCOUNT}"
```

On génère une clé secrète pour ce *service account*.
```bash
gcloud iam service-accounts keys create service_account_credentials.json --project ${GCP_PROJECT_ID} --iam-account ${GCP_SERVICE_ACCOUNT}
```

### Enable `GCP` *API*

🚧 TBD

### Installation de `CrossPlane` dans `MiniKube`
On installe `CrossPlane` via `Helm`.
```bash
kubectl create namespace crossplane-system
helm repo add crossplane-stable https://charts.crossplane.io/stable
helm repo update
helm install crossplane --namespace crossplane-system crossplane-stable/crossplane

# On vérifie que tout a été installé correctement.
helm list -n crossplane-system
kubectl get all -n crossplane-system
```

On créé un *secret* dans `K8s` pour stocker la clé du *service account* `GCP`.
```bash
kubectl create secret generic gcp-service-account-credentials \
    --namespace="crossplane-system" \
    --from-file=file-content=${HOME}/crossplane-anthos/service_account_credentials.json
```

On installe la *CLI* `CrossPlane` (il s'agit d'un plugin à `kubectl`).
```bash
curl -sL https://raw.githubusercontent.com/crossplane/crossplane/master/install.sh | sh
sudo mv crossplane /usr/local/bin
crossplane --help
```

### Visualisation des resources créées par CrossPlane
```bash
kubectl api-resources  | grep crossplane
```

# 1er provisionning de ressource `GCP` avec `CrossPlane`
Avec `CrossPlane`, on va provisionner une 1ère ressource `GCP` : un simple *bucket* `Google Cloud Storage` en chargeant les *manifests* `CrossPlane` dans `Minikube`.  
```bash
cd ${HOME}/crossplane-anthos/infrastructure/bootstrap

# Installation des providers GCP
kubectl apply -f providers.yaml
kubectl get providers

# Installation de la configuration du provider GCP avec fourniture du secret
kubectl apply -f provider-config.yaml

# Déploiement du bucket
echo "crossplane-bucket-"$(head -n 4096 /dev/urandom | openssl sha1 | tail -c 10)
kubectl apply -f bucket.yaml

kubectl get bucket.storage.gcp.upbound.io
```

### *Bootstrap* de la `Anthos` *fleet* avec un 1er *cluster* `GKE`
On provisionne un 1er *cluster* `GKE` que l'on enregistre à la `Anthos` *fleet*.
```bash
kubectl apply -f providers-gke.yaml

# Déploiement du cluster GKE et enregistrement dans la Anthos fleet
kubectl apply -f gke-cluster-1.yaml
kubectl get cluster.container.gcp.upbound.io
kubectl get membership.gkehub.gcp.upbound.io

gcloud container hub memberships list
gcloud container fleet features list
```

Pour que ce *cluster* puisse s'auto-configurer, on a besoin de fournir une source de configuration *GitOps* à l'outil `Anthos Config Management` (`Config-Sync`).  
Pour cela, on va utiliser le dossier `anthos-config` de notre dépôt `Git`.  
Côté Gitlab, on crée un *personal access token*… que l'on stocke dans un *secret* sur le cluster `GKE`.
```bash
# On configure kubectl pour qu'il s'adresse à notre cluster GKE
gcloud container clusters get-credentials anthos-gke-cluster-1 --zone ${GCP_ZONE}
kubectl config get-contexts
kubectl get nodes

# On créé un secret de connexion à Gitlab
kubectl create ns config-management-system
kubectl create secret generic git-creds \
  --namespace="config-management-system" \
  --from-literal=username=${GITLAB_USERNAME} \
  --from-literal=token=$(cat ${HOME}/crossplane-anthos/gitlab-access-token)
```

On configure la source de vérité de `Config-Sync` depuis le dépôt `Gitlab`
```bash
cd ${HOME}
gcloud beta container fleet config-management apply                       \
      --membership=anthos-gke-cluster-1                                   \
      --config=${HOME}/crossplane-anthos/infrastructure/bootstrap/apply-spec.yaml \
      --project=${GCP_PROJECT_ID}

gcloud beta container fleet config-management status \
    --project=${GCP_PROJECT_ID}
```

### Install Bank of Anthos
```bash
gcloud iam service-accounts create config-connector-sa
gcloud projects add-iam-policy-binding ${GCP_PROJECT_ID} \
  --member="serviceAccount:config-connector-sa@${GCP_PROJECT_ID}.iam.gserviceaccount.com" \
  --role="roles/owner"
gcloud iam service-accounts add-iam-policy-binding config-connector-sa@${GCP_PROJECT_ID}.iam.gserviceaccount.com \
  --member="serviceAccount:${GCP_PROJECT_ID}.svc.id.goog[cnrm-system/cnrm-controller-manager]" \
  --role="roles/iam.workloadIdentityUser"

# Create the Config Connector
kubectl apply -f ${HOME}/crossplane-anthos/infrastructure/bootstrap/config-connector.yaml

# Choose where to create resource
kubectl create namespace config-connector
kubectl annotate namespace config-connector cnrm.cloud.google.com/project-id=lgu-demos

# Change to branch config-sync-1
gcloud beta container fleet config-management apply                       \
      --membership=anthos-gke-cluster-1                                   \
      --config=${HOME}/crossplane-anthos/infrastructure/bootstrap/apply-spec.yaml \
      --project=${GCP_PROJECT_ID}
```

# Sources

- Google Kubernetes Engine with Crossplane - https://ce.qwiklabs.com/classrooms/9396/labs/65084
- Une explication de la cinématique GitOps avec Anthos, Crossplane et ArgoCD -  https://medium.com/@vincn.ledan/construisez-une-plateforme-moderne-avec-crossplane-anthos-et-argocd-lavenir-de-la-gestion-a73e8631afd8
- https://www.googlecloudcommunity.com/gc/Cloud-Events/Managing-multicloud-deployments-with-Anthos-and-Crossplane/ec-p/495591
- CrossPlane XRD and Composition - https://prune998.medium.com/playing-with-crossplane-for-real-f591e66065ae


## Installation de krew

https://krew.sigs.k8s.io/docs/user-guide/setup/install/

## Installation de krew/get-all

https://github.com/corneliusweig/ketall


